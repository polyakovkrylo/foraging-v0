import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
import matplotlib.pyplot as plt
from stl import mesh
from mpl_toolkits import mplot3d
import os

class ForagingEnv(gym.Env):
    metadata = {'render.modes':['human']}

    def __init__(self, num_agents=3, map_size=[10, 10, 10], fixed_goals=[]):
        # Initialize the bounding box defining the state space,
        # action space,  state, goals, rewards and done flags
        # (arrays of the size of the number of agents)
        self.bb = np.array([[0,0,0], map_size], dtype=np.int32)
        self.num_agents = num_agents
        self.action_space = [spaces.Discrete(7) for i in range(self.num_agents)]
        self.state = [np.array([0,0,0], dtype=np.int32) for i in range(self.num_agents)]
        self.fixed_goals = fixed_goals
        self.goal = [np.array([0,0,0], dtype=np.int32) for i in range(self.num_agents)]
        self.reward = [0 for i in range(self.num_agents)]
        self.done = [False for i in range(self.num_agents)]

        # Plotting routine initialization
        self.fig = plt.figure()
        self.ax = mplot3d.Axes3D(self.fig)
        self.fig.add_axes(self.ax)
        self.ax.set_xlim3d(self.bb[0,0]-1, self.bb[1,0]+1)
        self.ax.set_ylim3d(self.bb[0,1]-1, self.bb[1,1]+1)
        self.ax.set_zlim3d(self.bb[0,2]-1, self.bb[1,2]+1)
        self.agent_poly = []
        self.goal_poly = []
        plt.ion()

        # Create a poly instance for each agent and each goal
        self.mesh = mesh.Mesh.from_file(os.path.dirname(__file__)+'/mesh/quadcopter.stl')
        for i in range(self.num_agents):
            self.agent_poly.append(mplot3d.art3d.Poly3DCollection(
                self.mesh.vectors
            ))
            self.ax.add_collection(self.agent_poly[i])

            self.goal_poly.append(mplot3d.art3d.Poly3DCollection(
                self.mesh.vectors
            ))
            self.ax.add_collection(self.goal_poly[i])
            self.goal_poly[i].set_facecolor('r')

        # Reset the state
        self.reset()
        
    def is_occupied(self, target_point, occupied_points) :
        if len(occupied_points) == 0:
            return False
        else:
            occupancy = [np.all(target_point == p) for p in occupied_points]
            return np.any(occupancy)

    def generate_unique_point(self, occupied_points):
        # Generate a random unoccupied point
        while(True):
            point = np.array(self.bb[1] * np.random.rand(3), dtype=np.int32)
            
            if not self.is_occupied(point, occupied_points):
                return point

    def animate(self, frame, trajectories):
        for i in range(self.num_agents):
            self.agent_poly[i].set_verts(self.mesh.vectors*2 + trajectories[i][frame])

    def step(self, action):
        # Move each agent according to the chosen action
        for i, a in enumerate(action):
            agent_state = self.state[i].copy()
            if a == 0:
                agent_state[0] += 1
            if a == 1:
                agent_state[0] -= 1
            if a == 2:
                agent_state[1] += 1
            if a == 3:
                agent_state[1] -= 1
            if a == 4:
                agent_state[2] += 1
            if a == 5:
                agent_state[2] -= 1

            # We can only move to the next voxel if it's not occupied
            # and if it's not out of bounds
            if not self.is_occupied(agent_state, [s for s in self.state if np.any(s != self.state[i]) ] ):
                self.state[i] = agent_state
                self.state[i] = np.maximum(self.state[i], self.bb[0])
                self.state[i] = np.minimum(self.state[i], self.bb[1])

            # If an agent reached the goal return 0, otherwise return -1
            goal_found = self.is_occupied(self.state[i], self.goal)
            if goal_found:
                self.reward[i] = 0
                self.done[i] = True
            else:
                self.reward[i] = -1
                self.done[i] = False
            
        return [self.state, self.reward, self.done, self.info]
                
    def reset(self):
        # Generate goals
        for i in range(self.num_agents):
            # Take a fixed position if it exists, otherwise generate randomly
            if i+1 <= len(self.fixed_goals):
                self.goal[i] = self.fixed_goals[i]
            else:
                self.goal[i] = self.generate_unique_point([
                    g for g in self.goal if np.any(g != self.goal[i])])

        # Generate agents' initial positions
        for i in range(self.num_agents):
            self.state[i] = self.generate_unique_point(np.concatenate(
                ([self.state[j] for j in range(i)],
                self.goal) if i > 0 else [self.goal]
                ))

            # Reset rewards and done flags
            self.reward[i] = 0
            self.done[i] = False
        
            # Reset goal poly objects
            self.goal_poly[i].remove()
            self.goal_poly[i] =  mplot3d.art3d.Poly3DCollection(
                self.mesh.vectors*2+self.goal[i]
            )
            self.goal_poly[i].set_facecolor('r')
            self.ax.add_collection(self.goal_poly[i])

        # Reset meta dict
        self.prev_animation_state = self.state.copy()
        self.info = dict()
        return self.state

    def render(self):
        trajectories = []
        for i in range(self.num_agents):
            l1 = np.linspace(0, 1, 2)
            traj = (self.state[i].astype(np.float64)-self.prev_animation_state[i].astype(np.float64))*l1[:,None]
            trajectories.append(self.prev_animation_state[i].astype(np.float64) + traj)
        # Color target green if it is occupied
        [self.goal_poly[i].set_facecolor('g') for i in self.done if self.done[i]]

        # Hold the plot if all targets were acquired
        if np.all(self.done):
            hold = True
        else:
            hold = False

        for i in range(2):
            self.animate(i, trajectories)
            plt.show(block=hold)
            plt.pause(0.001)

        self.prev_animation_state = self.state.copy()
              

        
