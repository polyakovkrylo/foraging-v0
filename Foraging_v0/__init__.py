from gym.envs.registration import register

register(
    id='Foraging-v0',
    entry_point='Foraging_v0.envs:ForagingEnv',
    kwargs={'num_agents': 1,
            'map_size': [10, 10, 10],
            'fixed_goals': []}
    )
